package fall2011.cloud.shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ImagesServiceFailureException;
import com.google.appengine.api.images.Transform;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

public class ImageUtil {
	
	public static byte[] getImage(String imgurl) throws MalformedURLException, IOException, Exception{
		// System.out.println("Getting Image from url - "+ imgurl);
		URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
		byte[] imgBytes = urlFetchService.fetch(new URL(imgurl)).getContent();

		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		Image img = ImagesServiceFactory.makeImage(imgBytes);

		Transform transform = ImagesServiceFactory.makeRotate(90);
		Image newImg = imagesService.applyTransform(transform, img);
		transform = ImagesServiceFactory.makeResize(100, 100);
		newImg = imagesService.applyTransform(transform, newImg);

		return newImg.getImageData();
	}
	
	public static String getBlogImageUrl_test(String imgurl) throws Exception {
		byte[] img = getImage(imgurl);
		FileService fileService = FileServiceFactory.getFileService();
		AppEngineFile file = fileService.createNewBlobFile("image/jpg");

		// Open a channel to write to it
		boolean lock = true;
		FileWriteChannel writeChannel = fileService.openWriteChannel(file, lock);
		writeChannel.write(ByteBuffer.wrap(img));
		writeChannel.closeFinally();

		BlobKey blobKey = fileService.getBlobKey(file);
		while (blobKey == null) {
			blobKey = fileService.getBlobKey(file);
			System.out.println("Waiting for blobkey");
		}
		System.out.println("imageUrl: " + imgurl);
		System.out.println("blobKey: " + blobKey.getKeyString());

		return blobKey.getKeyString();
	}
	
	public static String getUrlFromBlobKey(String blobKey){
		try{
			String blobImageUrl = ImagesServiceFactory.getImagesService().getServingUrl(new BlobKey(blobKey));
			System.out.println("blobImageUrl: " + blobImageUrl);
			return blobImageUrl;
		} catch (ImagesServiceFailureException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getBlobImageUrl(String imgurl) throws IOException{
		InputStream imgIS = URLUtil.fetchUrlSync(imgurl);
		BufferedReader in = new BufferedReader(new InputStreamReader(imgIS));
		StringBuilder sb = new StringBuilder();
		String s = "";
		while ((s = in.readLine()) != null) {
			sb.append(s);
		}
		
		String imageString=sb.toString();
		
		FileService fileService = FileServiceFactory.getFileService();

		AppEngineFile file = fileService.createNewBlobFile("image/gif");

		FileWriteChannel writeChannel = fileService.openWriteChannel(file, true);
		PrintWriter out = new PrintWriter(Channels.newWriter(writeChannel, "UTF8"));
		out.println(imageString);
		out.close();
		writeChannel.closeFinally();

		BlobKey blobKey = fileService.getBlobKey(file);
		while (blobKey == null) {
			blobKey = fileService.getBlobKey(file);
			System.out.println("Waiting for blobkey");
		}

		try {
			ImagesService imageService = ImagesServiceFactory.getImagesService();
			String imageServingUrl = imageService.getServingUrl(blobKey);
			System.out.println("blobkey: " + blobKey.toString());
			System.out.println("imageServingUrl: " + imageServingUrl);
			return imageServingUrl;
		} catch (ImagesServiceFailureException e) {
			e.printStackTrace();
		}
		
		System.out.println("blobkey: " + blobKey.toString());
		System.out.println("imageServingUrl: noURL");
		return "noURL";
	}

}
