package fall2011.cloud.shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLUtil {

	public static Location parse(String woeid, InputStream inpStreamXML){
		try {
			Location location = new Location();
			location.setWoeid(woeid);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			inpStreamXML.mark(inpStreamXML.available()*2);
			inpStreamXML.reset();
			
			Document doc = db.parse(inpStreamXML);
			
			location.setTitle(doc.getElementsByTagName("item").item(0).getFirstChild().getTextContent());
			if(location.getTitle().equalsIgnoreCase("City not found")){
				return null;
			}
			
			NodeList yweatherLocation = doc.getElementsByTagName("yweather:location");
			Node yweatherLocationNode = yweatherLocation.item(0);
			location.setCity(yweatherLocationNode.getAttributes().getNamedItem("city").getNodeValue());
			location.setRegion(yweatherLocationNode.getAttributes().getNamedItem("region").getNodeValue());
			location.setCountry(yweatherLocationNode.getAttributes().getNamedItem("country").getNodeValue());
			
			NodeList yweatherAtmosphere = doc.getElementsByTagName("yweather:atmosphere");
			Node yweatherAtmosphereNode = yweatherAtmosphere.item(0);
			location.setHumidity(yweatherAtmosphereNode.getAttributes().getNamedItem("humidity").getNodeValue());
			location.setVisibility(yweatherAtmosphereNode.getAttributes().getNamedItem("visibility").getNodeValue());
			 
			NodeList yweatherAstronomy = doc.getElementsByTagName("yweather:astronomy");
			Node yweatherAstronomyNode = yweatherAstronomy.item(0);
			location.setSunrise(yweatherAstronomyNode.getAttributes().getNamedItem("sunrise").getNodeValue());
			location.setSunset(yweatherAstronomyNode.getAttributes().getNamedItem("sunset").getNodeValue());
			
			NodeList yweatherCondition = doc.getElementsByTagName("yweather:condition");
			Node yweatherConditionNode = yweatherCondition.item(0);
			location.setTemperature(Double.parseDouble((yweatherConditionNode.getAttributes().getNamedItem("temp").getNodeValue()).trim()));
			location.setTime(yweatherConditionNode.getAttributes().getNamedItem("date").getNodeValue());
			
			inpStreamXML.reset();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(inpStreamXML));
			StringBuilder sb = new StringBuilder();
			String s = "";
			while ((s = in.readLine()) != null) {
				sb.append(s);
			}
			
			int start = sb.toString().indexOf("img src=");
			int end = sb.toString().indexOf('"', start+12);
			String temp = sb.toString().substring(start+9, end);
			location.setImgUrl(temp);
			
			return location;
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}

