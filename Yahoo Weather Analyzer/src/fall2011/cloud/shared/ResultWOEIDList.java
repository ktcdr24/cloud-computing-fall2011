package fall2011.cloud.shared;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ResultWOEIDList implements IsSerializable {

	private ArrayList<Location> hottestLocations;
	private ArrayList<Location> coldestLocations;
	
	public ResultWOEIDList() {
	}
	
	public ResultWOEIDList(ArrayList<Location> hottestLocations, ArrayList<Location> coldestLocations) {
		this.hottestLocations = hottestLocations;
		this.coldestLocations = coldestLocations;
	}
	
	public ArrayList<Location> getHottestLocations() {
		return hottestLocations;
	}
	public void setHottestLocations(ArrayList<Location> hottestLocations) {
		this.hottestLocations = hottestLocations;
	}
	public ArrayList<Location> getColdestLocations() {
		return coldestLocations;
	}
	public void setColdestLocations(ArrayList<Location> coldestLocations) {
		this.coldestLocations = coldestLocations;
	}

	public List<String> getWOEIDList(){
		List<String> woeidList=new ArrayList<String>();
		for(Location l:hottestLocations){
			woeidList.add(l.getWoeid());
		}
		for(Location l:coldestLocations)
			woeidList.add(l.getWoeid());

		return woeidList;
	}
	
	public String getMailMessage(){
		StringBuilder sb = new StringBuilder();
		sb.append(" \n Hottest Locations: \n");
		for(Location l : hottestLocations){
			sb.append(l + "\n");
		}
		sb.append(" \n Coldest Locations: \n");
		for(Location l:coldestLocations){
			sb.append(l + "\n");
		}
		return sb.toString();
	}
	
}
