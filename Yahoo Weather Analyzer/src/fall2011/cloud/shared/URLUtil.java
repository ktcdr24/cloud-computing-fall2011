package fall2011.cloud.shared;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.urlfetch.URLFetchServicePb.URLFetchRequest;
import com.google.appengine.api.urlfetch.URLFetchServicePb.URLFetchRequest.Builder;
import com.google.appengine.api.urlfetch.URLFetchServicePb.URLFetchResponse;
import com.google.appengine.api.urlfetch.URLFetchServicePb.URLFetchService;
import com.google.appengine.repackaged.com.google.protobuf.RpcCallback;
import com.google.appengine.repackaged.com.google.protobuf.RpcController;

public class URLUtil {

	/**
	 * @param urlStr
	 */
	public static InputStream fetchUrlSync(String urlStr) {
		URL search;
		try {
			search = new URL(urlStr);
			URLConnection conn = search.openConnection();
//			BufferedReader in = new BufferedReader(new InputStreamReader(
//					conn.getInputStream()));
//			StringBuilder sb = new StringBuilder();
//			String s = "";
//			while ((s = in.readLine()) != null) {
//				sb.append(s);
//			}
			return conn.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public static void fetchUrlAsync(){
		Builder urlBuilder=URLFetchRequest.newBuilder();
		urlBuilder.setUrl("http://weather.yahooapis.com/forecastrss?w=2442047&u=c");
		URLFetchRequest urlFetchRequest= urlBuilder.build();
		
		URLFetchService ufs=(URLFetchService) URLFetchServiceFactory.getURLFetchService();
		ufs.fetch(rpcController, urlFetchRequest, rpcCallback);
		
	}
		
	static RpcCallback<URLFetchResponse> rpcCallback=new RpcCallback<URLFetchResponse>() {

		@Override
		public void run(URLFetchResponse arg0) {
			// TODO Auto-generated method stub
			 com.google.appengine.repackaged.com.google.protobuf.ByteString content=arg0.getContent();
			 System.out.println(content.toString());
		}
	};
	
	static RpcController rpcController=new RpcController() {
		
		@Override
		public void startCancel() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void setFailed(String arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void reset() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void notifyOnCancel(RpcCallback<Object> arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public boolean isCanceled() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public boolean failed() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public String errorText() {
			// TODO Auto-generated method stub
			return null;
		}
	};
	
}
