package fall2011.cloud.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Location implements IsSerializable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6354524415376769808L;

	public static String TITLE="title";
	public static String CITY="city";
	public static String REGION="region";
	public static String COUNTRY="country";
	public static String HUMIDITY="humidity";
	public static String VISIBILITY="visibility";
	public static String SUNRISE="sunsire";
	public static String SUNSET="sunset";
	public static String IMGURL="imgurl";
	public static String TEMPERATURE="temperature";
	public static String WOEID="woeid";
	public static String TIME="time";
	
	private String title;
	private String city; 
	private String region;
	private String country;
	private String humidity;
	private String visibility;
	private String sunrise;
	private String sunset;
	private String imgUrl;
	private String woeid;
	private Double temperature;
	private String time;
	private String imgBase64Str = null;
	private byte[] imgBytAry = null;
	private String blobKey = null;
	
	public void setWoeid(String woeid) {
		this.woeid = woeid;
	}

	public Location() {
		this.title = null;
		this.city = null;
		this.region = null;
		this.country = null;
		this.humidity = null;
		this.visibility = null;
		this.sunrise = null;
		this.sunset = null;
		this.imgUrl = null;
		this.temperature = null;
		this.woeid = null;
		this.time = null;
	}
	
	public Location(String woeid) {
		this.title = null;
		this.city = null;
		this.region = null;
		this.country = null;
		this.humidity = null;
		this.visibility = null;
		this.sunrise = null;
		this.sunset = null;
		this.imgUrl = null;
		this.temperature = null;
		this.woeid=woeid;
		this.time = null;
	}
	
	public String getWoeid(){
		return woeid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String getSunrise() {
		return sunrise;
	}
	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}
	public String getSunset() {
		return sunset;
	}
	public void setSunset(String sunset) {
		this.sunset = sunset;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getImgBase64Str() {
		return imgBase64Str;
	}
	public void setImgBase64Str(String imgBase64Str) {
		this.imgBase64Str = imgBase64Str;
	}
	public byte[] getImgBytAry() {
		return imgBytAry;
	}
	public void setImgBytAry(byte[] imgBytAry) {
		this.imgBytAry = imgBytAry;
	}
	public String getBlobKey() {
		return blobKey;
	}
	public void setBlobKey(String blobKey) {
		this.blobKey = blobKey;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" WOEID: " + getWoeid()).append(", City: " + getCity()).append(", Region: " + getRegion()).append(", Country: " + getCountry());
		sb.append(", Temperature: " + getTemperature()).append(", Humidity: " + getHumidity()).append(", Visibility: " + getVisibility());
		sb.append(", Sunrise: " + getSunrise()).append(", Sunset: " + getSunset());
		return sb.toString();
	}
}
