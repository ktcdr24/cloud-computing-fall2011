/**
 * 
 */
package fall2011.cloud.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fall2011.cloud.shared.ResultWOEIDList;

/**
 * @author Lenovo
 *
 */
@RemoteServiceRelativePath("yahooWeather")
public interface YahooWeatherService extends RemoteService {
	ResultWOEIDList getTopTempCitiesAndSendMail() throws IllegalArgumentException;
	void workAround(String str);
}
