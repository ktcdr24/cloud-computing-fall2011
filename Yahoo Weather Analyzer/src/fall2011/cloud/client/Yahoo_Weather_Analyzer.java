package fall2011.cloud.client;

import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fall2011.cloud.shared.Location;
import fall2011.cloud.shared.ResultWOEIDList;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Yahoo_Weather_Analyzer implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	private final YahooWeatherServiceAsync yahooWeatherServiceAsync = GWT
			.create(YahooWeatherService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final Button sendButton = new Button("Send");
		final Button yahooWeatherButton = new Button("Yahoo! Weather");
		final Label errorLabel = new Label();
		final FlexTable hotTempTable = new FlexTable();
		final VerticalPanel mainPanel = new VerticalPanel();
		final FlexTable coldTempTable = new FlexTable();
		final VerticalPanel mainPanel2 = new VerticalPanel();
		final Label lastUpdatedLabel = new Label();
		final Label successLabel = new Label();
		
		// We can add style names to widgets
		sendButton.addStyleName("sendButton");
		yahooWeatherButton.addStyleName("sendButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("yahooButtonContainer").add(yahooWeatherButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("hottestTempTable").add(mainPanel);
		RootPanel.get("coldestTempTable").add(mainPanel2);
		RootPanel.get("successContainer").add(successLabel);
		RootPanel.get("lastUpdateContainer").add(lastUpdatedLabel);
		
		lastUpdatedLabel.setText("Last update at none");
		
		hotTempTable.setBorderWidth(1);

		coldTempTable.setBorderWidth(1);
		
		mainPanel.add(hotTempTable);
		hotTempTable.setVisible(false);
		mainPanel2.add(coldTempTable);
		coldTempTable.setVisible(false);
		
		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				yahooWeatherButton.setEnabled(true);
				yahooWeatherButton.setFocus(true);
			}
		});

		class YahooWeatherButtonHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				yahooWeatherButton.setEnabled(false);
				hotTempTable.setVisible(false);
				coldTempTable.setVisible(false);
				successLabel.setText("Processing... Please Wait");
				lastUpdatedLabel.setText("");
				try{
				yahooWeatherServiceAsync.getTopTempCitiesAndSendMail(						
						new AsyncCallback<ResultWOEIDList>() {
							public void onFailure(Throwable caught) {
								// Show the RPC error message to the user
								dialogBox.setText("Remote Procedure Call - Failure");
								serverResponseLabel.addStyleName("serverResponseLabelError");
								serverResponseLabel.setHTML(SERVER_ERROR);
								dialogBox.center();
								closeButton.setFocus(true);
								
								successLabel.setText("Failed!!!");
							}

							public void onSuccess(ResultWOEIDList result) {
								//dialogBox.setText("Remote Procedure Call");
								//serverResponseLabel.removeStyleName("serverResponseLabelError");
								//serverResponseLabel.setHTML("success");
								//dialogBox.center();
								//closeButton.setFocus(true);
								yahooWeatherButton.setEnabled(true);
								hotTempTable.setVisible(true);
								coldTempTable.setVisible(true);
								successLabel.setText("Success!!!");
								hotTempTable.setTitle("Hottest Locations");
								hotTempTable.setText(0, 0, "WOEID");
								hotTempTable.setText(0, 1, "City");
								hotTempTable.setText(0, 2, "Temperature");
								hotTempTable.setText(0, 3, "Region");
								hotTempTable.setText(0, 4, "Country");
								hotTempTable.setText(0, 5, "Humidity");
								hotTempTable.setText(0, 6, "Visibility");
								hotTempTable.setText(0, 7, "Sunrise");
								hotTempTable.setText(0, 8, "Sunset");
								hotTempTable.setText(0, 9, "Image");
								hotTempTable.setText(0, 10, "Processed Image");
								coldTempTable.setTitle("Coldest Locations");
								coldTempTable.setText(0, 0, "WOEID");
								coldTempTable.setText(0, 1, "City");
								coldTempTable.setText(0, 2, "Temperature");
								coldTempTable.setText(0, 3, "Region");
								coldTempTable.setText(0, 4, "Country");
								coldTempTable.setText(0, 5, "Humidity");
								coldTempTable.setText(0, 6, "Visibility");
								coldTempTable.setText(0, 7, "Sunrise");
								coldTempTable.setText(0, 8, "Sunset");
								coldTempTable.setText(0, 9, "Image");
								coldTempTable.setText(0, 10, "Processed Image");
								lastUpdatedLabel.setText("Last Updated: " + new Date().toString());
								if(result!=null){
									if(result.getHottestLocations() != null){
										int count = 1;
										for(Location loc : result.getHottestLocations()){
											hotTempTable.setText(count, 0, loc.getWoeid());
											hotTempTable.setText(count, 1, loc.getCity());
											hotTempTable.setText(count, 2, loc.getTemperature().toString());
											hotTempTable.setText(count, 3, loc.getRegion());
											hotTempTable.setText(count, 4, loc.getCountry());
											hotTempTable.setText(count, 5, loc.getHumidity());
											hotTempTable.setText(count, 6, loc.getVisibility());
											hotTempTable.setText(count, 7, loc.getSunrise());
											hotTempTable.setText(count, 8, loc.getSunset());
											hotTempTable.setWidget(count, 9, new Image(loc.getImgUrl()));
											hotTempTable.setWidget(count, 10, new Image(loc.getImgBase64Str()));
											count++;	
										}
									}
									if(result.getColdestLocations() != null){
										int count = 1;
										for(Location loc : result.getColdestLocations()){
											coldTempTable.setText(count, 0, loc.getWoeid());
											coldTempTable.setText(count, 1, loc.getCity());
											coldTempTable.setText(count, 2, loc.getTemperature().toString());
											coldTempTable.setText(count, 3, loc.getRegion());
											coldTempTable.setText(count, 4, loc.getCountry());
											coldTempTable.setText(count, 5, loc.getHumidity());
											coldTempTable.setText(count, 6, loc.getVisibility());
											coldTempTable.setText(count, 7, loc.getSunrise());
											coldTempTable.setText(count, 8, loc.getSunset());
											coldTempTable.setWidget(count, 9, new Image(loc.getImgUrl()));
											coldTempTable.setWidget(count, 10, new Image(loc.getImgBase64Str()));
											count++;
										}
									}
								}
							}
						});
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
		}
		
		yahooWeatherButton.addClickHandler(new YahooWeatherButtonHandler());
		
	}
}
