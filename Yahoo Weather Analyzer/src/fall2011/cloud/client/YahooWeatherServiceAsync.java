package fall2011.cloud.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fall2011.cloud.shared.ResultWOEIDList;


public interface YahooWeatherServiceAsync {

	void getTopTempCitiesAndSendMail(AsyncCallback<ResultWOEIDList> callback);

	void workAround(String str, AsyncCallback<Void> callback);

}
