/**
 * 
 */
package fall2011.cloud.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fall2011.cloud.client.YahooWeatherService;
import fall2011.cloud.shared.ImageUtil;
import fall2011.cloud.shared.Location;
import fall2011.cloud.shared.ResultWOEIDList;
import fall2011.cloud.shared.URLUtil;
import fall2011.cloud.shared.XMLUtil;

/**
 * @author Lenovo
 * 
 */
public class YahooWeatherServiceImpl extends RemoteServiceServlet implements
		YahooWeatherService {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -73349815726700222L;

	public static final String LIST_KEY="listKey";
	public static final String QUERY_KEY = "trial5";
	
	private Map<String, Location> locMap = new HashMap<String, Location>();
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fall2011.cloud.client.YahooWeatherService#getTopTempCitiesAndSendMail()
	 */
	@Override
	public ResultWOEIDList getTopTempCitiesAndSendMail()
			throws IllegalArgumentException {
		System.out.println(" KANNA ");
		
		String[] woeid1 = new String[] { "2379574", "2502265", "2442047",
				"12796932", "2471217", "2487796", "2388929", "2424766",
				"2406080", "2357024", "2459115", "2367105", "2487956",
				"2358820", "2490383", "2514815", "455827", "44418", "2461928",
				"2354302", "2354842", "2351492", "2354490", "2378015",
				"2367851", "2358492", "2363796", "2404850", "2367231",
				"2358751", "2347567", "2459271", "2347568", "2404462",
				"2352648", "2416073", "240485035", "2355448", "2357283",
				"2431650", "2365649", "2524465", "2351526", "2416565",
				"2515315", "2399741", "2378319", "12590237", "2443945",
				"2351723" };

		Location location = null;
		for (String s : woeid1) {
			String urlStr = "http://weather.yahooapis.com/forecastrss?w=" + s;
			InputStream inpStreamXML = URLUtil.fetchUrlSync(urlStr);
			if (inpStreamXML != null) {
				location = XMLUtil.parse(s, inpStreamXML);
				if(location !=null){
					try {
						location.setBlobKey(ImageUtil.getBlogImageUrl_test(location.getImgUrl()));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					storeLocationInDataStore(location);
					locMap.put(location.getWoeid(), location);
					System.out.println("\n Stored location: "+location);
				} else {
					System.out.println("\n Unstored location: "+location);
				}
			}
			urlStr = null;
		}

		System.out.println(" KANNA1 ");

		try{
			ArrayList<Location> hottestLocations = retrieveTop5HottestWOEIDS();
			ArrayList<Location> coldestLocations = retrieveTop5ColdestWOEIDS();
			
			for(Location l : hottestLocations){
				l.setImgBase64Str(ImageUtil.getUrlFromBlobKey(l.getBlobKey()));
			}
			for(Location l : coldestLocations){
				l.setImgBase64Str(ImageUtil.getUrlFromBlobKey(l.getBlobKey()));
			}
			
			ResultWOEIDList toRet = new ResultWOEIDList(hottestLocations, coldestLocations);

			putInMemCache(toRet);
			sendMail(toRet);
			
			return toRet;
		}catch(Exception e){
			e.printStackTrace();	
		}

		return null;
	}

	public ArrayList<Location> retrieveTop5HottestWOEIDS() {
		Query query = new Query(QUERY_KEY).addSort(Location.TEMPERATURE, Query.SortDirection.DESCENDING);
		DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> entities = dataStore.prepare(query).asList(FetchOptions.Builder.withLimit(5));

		ArrayList<Location> hottestLocations = new ArrayList<Location>();
		Location loc = null;
		for (Entity e : entities) {
			loc = new Location();
			loc.setWoeid(e.getProperty(Location.WOEID) + "");
			loc.setCity(e.getProperty(Location.CITY) + "");
			loc.setCountry(e.getProperty(Location.COUNTRY) + "");
			loc.setHumidity(e.getProperty(Location.HUMIDITY) + "");
			loc.setImgUrl(e.getProperty(Location.IMGURL) + "");
			loc.setRegion(e.getProperty(Location.REGION) + "");
			loc.setSunrise(e.getProperty(Location.SUNRISE) + "");
			loc.setSunset(e.getProperty(Location.SUNSET) + "");
			loc.setTime(e.getProperty(Location.TIME) + "");
			loc.setVisibility(e.getProperty(Location.VISIBILITY) + "");
			loc.setTemperature(Double.parseDouble(e.getProperty(Location.TEMPERATURE) + ""));
			
			loc = locMap.get(loc.getWoeid());
			hottestLocations.add(loc);
		}
		return hottestLocations;
	}
	
	public ArrayList<Location> retrieveTop5ColdestWOEIDS() {
		Query query = new Query(QUERY_KEY).addSort(Location.TEMPERATURE, Query.SortDirection.ASCENDING);
		DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> entities = dataStore.prepare(query).asList(FetchOptions.Builder.withLimit(5));

		ArrayList<Location> coldestLocations = new ArrayList<Location>();
		Location loc = null;
		for (Entity e : entities) {
			loc = new Location();
			loc.setWoeid(e.getProperty(Location.WOEID) + "");
			loc.setCity(e.getProperty(Location.CITY) + "");
			loc.setCountry(e.getProperty(Location.COUNTRY) + "");
			loc.setHumidity(e.getProperty(Location.HUMIDITY) + "");
			loc.setImgUrl(e.getProperty(Location.IMGURL) + "");
			loc.setRegion(e.getProperty(Location.REGION) + "");
			loc.setSunrise(e.getProperty(Location.SUNRISE) + "");
			loc.setSunset(e.getProperty(Location.SUNSET) + "");
			loc.setTime(e.getProperty(Location.TIME) + "");
			loc.setVisibility(e.getProperty(Location.VISIBILITY) + "");
			loc.setTemperature(Double.parseDouble(e.getProperty(Location.TEMPERATURE) + ""));
			
			loc = locMap.get(loc.getWoeid());
			coldestLocations.add(loc);
		}

		return coldestLocations;
	}

	public void storeLocationInDataStore(Location location) {
		if(location.getCity() == null || location.getCity().length() == 0){
			return;
		}
		Entity entity = new Entity(QUERY_KEY, location.getWoeid());
		entity.setProperty(Location.CITY, location.getCity());
		entity.setProperty(Location.COUNTRY, location.getCountry());
		entity.setProperty(Location.HUMIDITY, location.getHumidity());
		entity.setProperty(Location.IMGURL, location.getImgUrl());
		entity.setProperty(Location.REGION, location.getRegion());
		entity.setProperty(Location.SUNRISE, location.getSunrise());
		entity.setProperty(Location.SUNSET, location.getSunset());
		entity.setProperty(Location.TEMPERATURE, location.getTemperature());
		entity.setProperty(Location.TIME, location.getTime());
		entity.setProperty(Location.VISIBILITY, location.getVisibility());
		entity.setProperty(Location.WOEID, location.getWoeid());

		DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		dataStore.put(entity);
	}

	
	public void putInMemCache(ResultWOEIDList list){
		
		System.out.println("inside put in memcache");
		
		MemcacheService memcacheService = MemcacheServiceFactory.getMemcacheService();
		try {
			@SuppressWarnings("unchecked")
			List<String> woeidList=(List<String>)memcacheService.get(LIST_KEY);
			if(woeidList != null){
				memcacheService.deleteAll(woeidList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		memcacheService.put(LIST_KEY, list.getWOEIDList());
		for(Location l : list.getColdestLocations()){
			memcacheService.put(l.getWoeid(), l.getTemperature());
		}
		for(Location l : list.getColdestLocations()){
			memcacheService.put(l.getWoeid(), l.getTemperature());
		}
		
	}
	
	public void sendMail(ResultWOEIDList list){
		
		System.out.println("sending Mail");
		
		Properties properties = new Properties();
		Session session = Session.getDefaultInstance(properties, null);
		
		String msgBody = list.getMailMessage();
		Message message = new MimeMessage(session);
		try {
			
			message.setFrom(new InternetAddress("kanap008@gmail.com", "Kannapiran"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("anuj.sampath@gmail.com", "Anuj"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("kanap008@gmail.com", "Kanna"));
			message.setSubject("Cloud Computing Assignment 3 Result from kt2424 as4046");
			message.setText(msgBody);
			Transport.send(message);
			Transport.send(message, new InternetAddress[]{new InternetAddress("anuj.sampath@gmail.com")});
			Thread.sleep(500);	
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Mail message Body: " + msgBody);
		System.out.println("sent Mail. ");
	}
	
	@Override
	public void workAround(String str) {

	}

}
