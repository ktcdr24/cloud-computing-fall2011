/*
 * Copyright 2010-2011 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.DomainMetadataRequest;
import com.amazonaws.services.simpledb.model.DomainMetadataResult;
import com.amazonaws.services.simpledb.model.ListDomainsRequest;
import com.amazonaws.services.simpledb.model.ListDomainsResult;

/**
 * Welcome to your new AWS Java SDK based project!
 * 
 * This class is meant as a starting point for your console-based application
 * that makes one or more calls to the AWS services supported by the Java SDK,
 * such as EC2, SimpleDB, and S3.
 * 
 * In order to use the services in this sample, you need:
 * 
 * - A valid Amazon Web Services account. You can register for AWS at:
 * https://aws-portal.amazon.com/gp/aws/developer/registration/index.html
 * 
 * - Your account's Access Key ID and Secret Access Key:
 * http://aws.amazon.com/security-credentials
 * 
 * - A subscription to Amazon EC2. You can sign up for EC2 at:
 * http://aws.amazon.com/ec2/
 * 
 * - A subscription to Amazon SimpleDB. You can sign up for Simple DB at:
 * http://aws.amazon.com/simpledb/
 * 
 * - A subscription to Amazon S3. You can sign up for S3 at:
 * http://aws.amazon.com/s3/
 */
public class AwsConsoleApp {

	/*
	 * Important: Be sure to fill in your AWS access credentials in the
	 * AwsCredentials.properties file before you try to run this sample.
	 * http://aws.amazon.com/security-credentials
	 */

	static AmazonEC2 ec2;
	static AmazonS3 s3;
	static AmazonSimpleDB sdb;

	/**
	 * The only information needed to create a client are security credentials
	 * consisting of the AWS Access Key ID and Secret Access Key. All other
	 * configuration, such as the service endpoints, are performed
	 * automatically. Client parameters, such as proxies, can be specified in an
	 * optional ClientConfiguration object when constructing a client.
	 * 
	 * @see com.amazonaws.auth.BasicAWSCredentials
	 * @see com.amazonaws.auth.PropertiesCredentials
	 * @see com.amazonaws.ClientConfiguration
	 */
	private static void init() throws Exception {
		AWSCredentials credentials = new PropertiesCredentials(
				AwsConsoleApp.class
						.getResourceAsStream("AwsCredentials.properties"));

		ec2 = new AmazonEC2Client(credentials);
		s3 = new AmazonS3Client(credentials);
		sdb = new AmazonSimpleDBClient(credentials);
	}

	public static void main(String[] args) throws Exception {

		System.out.println("===========================================");
		System.out.println("Welcome to the AWS Java SDK!");
		System.out.println("===========================================");

		init();

		/*
		 * Amazon EC2
		 * 
		 * The AWS EC2 client allows you to create, delete, and administer
		 * instances programmatically.
		 * 
		 * In this sample, we use an EC2 client to get a list of all the
		 * availability zones, and all instances sorted by reservation id.
		 */
		try {
			DescribeAvailabilityZonesResult availabilityZonesResult = ec2
					.describeAvailabilityZones();
			System.out.println("You have access to "
					+ availabilityZonesResult.getAvailabilityZones().size()
					+ " Availability Zones.");

////////////////////////////////////////////////////////////////////////////////////////////////////////
// /// ASSIGNMENT 1 starts... Kannapiran Thirunarayanan(kt2424)
			
			CreateKeyPairRequest keyPairRequest = new CreateKeyPairRequest();
			keyPairRequest.setKeyName("kt2424");

			CreateKeyPairResult keyPairResult = ec2.createKeyPair(keyPairRequest);
			System.out.println("Key Pair created --> " + keyPairResult.toString());
			String kepPairPEM = keyPairResult.getKeyPair().getKeyMaterial();

			File keyFilePEM = null;
			try{
			keyFilePEM = new File("C:\\Putty\\kt2424.pem");
			keyFilePEM.createNewFile();
			FileWriter fw = new FileWriter(keyFilePEM);
			fw.write(kepPairPEM);
			fw.flush();
			fw.close();
			System.out.println("The Private Key PEM file is available at location -->  " + keyFilePEM.getAbsolutePath());
			} catch(IOException exe) {
				exe.printStackTrace();
			}

			// create security group...
			CreateSecurityGroupRequest securityGroupRequest = new CreateSecurityGroupRequest();
			securityGroupRequest.setGroupName("openPorts");
			securityGroupRequest.setDescription("created from EC2 API");
			
			AWSCredentials credentials = new PropertiesCredentials(AwsConsoleApp.class
							.getResourceAsStream("AwsCredentials.properties"));
			
			securityGroupRequest.setRequestCredentials(credentials);
			CreateSecurityGroupResult securityGroupResult = ec2.createSecurityGroup(securityGroupRequest);
			System.out.println("Sceurity Group Created - " + securityGroupResult.toString());
			
			String securityGroupID = securityGroupResult.getGroupId();
			// security group "openPorts" created.
			
			// open required ports in security group.
			AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest();
			
			IpPermission ipPermission = new IpPermission();
			ipPermission.setFromPort(22);	// ssh = 22, http = 80, https = 443 
			ipPermission.setToPort(444);
			List<String> ipRanges = new ArrayList<String>();
			ipRanges.add("0.0.0.0/0");		// allow all IP addresses...
			ipPermission.setIpRanges(ipRanges);
			ipPermission.setIpProtocol("tcp");
			
			List<IpPermission> ipPermissions = new ArrayList<IpPermission>();
			ipPermissions.add(ipPermission);
			
			authorizeSecurityGroupIngressRequest.setIpPermissions(ipPermissions);
			authorizeSecurityGroupIngressRequest.setRequestCredentials(credentials);
			authorizeSecurityGroupIngressRequest.setGroupId(securityGroupID);
			
			ec2.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
			System.out.println("The ports from " + ipPermission.getFromPort() + " till " + ipPermission.getToPort() 
					+ " for "	+ ipPermission.getIpProtocol() + " is authorized for Security Group --> " 
					+ securityGroupResult.toString());
			// "openPorts" authorised for TCP ports 22 to 444
			

			// get the security group name of the newly created security group "openPorts";
			List<String> securityGroupNames = new ArrayList<String>();
			DescribeSecurityGroupsResult securityGroupsResult = ec2.describeSecurityGroups();
			for (SecurityGroup sg : securityGroupsResult.getSecurityGroups()) {
				if(sg.getGroupId().equals(securityGroupID)){
					securityGroupNames.add(sg.getGroupName());
				}
//				System.out.println(" security gourp - " + sg.getDescription());
//				System.out.println(" security gourp - " + sg.getGroupId());
//				System.out.println(" security gourp - " + sg.getGroupName());
//				System.out.println(" security gourp - " + sg);
//				System.out.println(" security gourp - ends ----------------------------");
			}

			// create  runInstanceRequest Object
			String freeInstanceAMI = "ami-7341831a";
			String suse64AMI = "ami-e4a3578d";
			RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
			runInstancesRequest.setInstanceType("t1.micro");
			runInstancesRequest.setImageId(suse64AMI); // currently using
															// free tier machine
															// but use -->> SUSE
															// -
															// 64bit("ami-e4a3578d");
			runInstancesRequest.setMinCount(1);
			runInstancesRequest.setMaxCount(1);
			runInstancesRequest.setKeyName("kt2424");
			runInstancesRequest.setSecurityGroups(securityGroupNames);

			// run the Amazon EC2 instance.
			RunInstancesResult runInstancesResult = ec2.runInstances(runInstancesRequest);
			System.out.println(" Amazon EC2 Instance requested.");
			
			// get the publicDNS of the initiated EC2 instance and assign tags to instances.
			List<String> publicDNSAddressOfInstances = new ArrayList<String>();
			for (Instance instance : runInstancesResult.getReservation().getInstances()) {
				// add tags for easy identification
				List<String> resources = new LinkedList<String>();
				List<Tag> tags = new LinkedList<Tag>();
				resources.add(instance.getInstanceId());
				tags.add(new Tag("Name", "Kanna"));
				tags.add(new Tag("Source", "AmazonEC2_Java_Api"));
				ec2.createTags(new CreateTagsRequest(resources, tags));

				// remove terminated and shutting-down instances
				if(instance.getState().getName().equalsIgnoreCase("shutting-down") || instance.getState().getName().equalsIgnoreCase("terminated")){
					continue;
				}
				
				while(true){
					// if pending - wait, else - get out of the while loop.
					if(instance.getState().getName().equalsIgnoreCase("pending")){
						System.out.println("[" + new Date() + "] Instance "+ instance.getInstanceId() +" is still in '" + instance.getState().getName() + "' state");
						System.out.println(" Sleeping for 30 seconds ");
						for(int sec=30; sec>0; sec--){
							System.out.print(sec + ", ");
							Thread.sleep(1000);
						}
						DescribeInstancesRequest instancesRequest = new DescribeInstancesRequest();
						instancesRequest.setInstanceIds(new ArrayList<String>());
						instancesRequest.getInstanceIds().add(instance.getInstanceId());
						
						DescribeInstancesResult instancesResult = ec2.describeInstances(instancesRequest);
						instance = instancesResult.getReservations().get(0).getInstances().get(0);
						continue;
					} else if(instance.getState().getName().equalsIgnoreCase("running")){
						break;
					} 
				}
				
				System.out.println("[" + new Date() + "] Instance "+ instance.getInstanceId() +" is now in '" + instance.getState().getName() + "' state");
				System.out.println("Public DNS address of the instance " + instance.getInstanceId() + " is --->  " + instance.getPublicDnsName());
				System.out.println("You can SSH this public DNS to access the machine. Use the file at location " + keyFilePEM.getAbsolutePath() + " as the private key file for doing ssh." );
				
				publicDNSAddressOfInstances.add(instance.getPublicDnsName());
				
			}
			
// /// ASSIGNMENT 1 ends... Kannapiran Thirunarayanan(kt2424)			
////////////////////////////////////////////////////////////////////////////////////////////////////////

			DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
			List<Reservation> reservations = describeInstancesRequest.getReservations();
			Set<Instance> instances = new HashSet<Instance>();

			for (Reservation reservation : reservations) {
				instances.addAll(reservation.getInstances());
			}

			System.out.println("You have " + instances.size()
					+ " Amazon EC2 instance(s) running.");

			for (Instance instance : instances) {
				System.out.println("-->" + instance.toString());
			}
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////

		
			
		} catch (AmazonServiceException ase) {
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		}

		/*
		 * Amazon SimpleDB
		 * 
		 * The AWS SimpleDB client allows you to query and manage your data
		 * stored in SimpleDB domains (similar to tables in a relational DB).
		 * 
		 * In this sample, we use a SimpleDB client to iterate over all the
		 * domains owned by the current user, and add up the number of items
		 * (similar to rows of data in a relational DB) in each domain.
		 */
		try {
			ListDomainsRequest sdbRequest = new ListDomainsRequest()
					.withMaxNumberOfDomains(100);
			ListDomainsResult sdbResult = sdb.listDomains(sdbRequest);

			int totalItems = 0;
			for (String domainName : sdbResult.getDomainNames()) {
				DomainMetadataRequest metadataRequest = new DomainMetadataRequest()
						.withDomainName(domainName);
				DomainMetadataResult domainMetadata = sdb
						.domainMetadata(metadataRequest);
				totalItems += domainMetadata.getItemCount();
			}

			System.out.println("You have " + sdbResult.getDomainNames().size()
					+ " Amazon SimpleDB domain(s)" + "containing a total of "
					+ totalItems + " items.");
		} catch (AmazonServiceException ase) {
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		}

		/*
		 * Amazon S3
		 * 
		 * The AWS S3 client allows you to manage buckets and programmatically
		 * put and get objects to those buckets.
		 * 
		 * In this sample, we use an S3 client to iterate over all the buckets
		 * owned by the current user, and all the object metadata in each
		 * bucket, to obtain a total object and space usage count. This is done
		 * without ever actually downloading a single object -- the requests
		 * work with object metadata only.
		 */
		try {
			List<Bucket> buckets = s3.listBuckets();

			long totalSize = 0;
			int totalItems = 0;
			for (Bucket bucket : buckets) {
				/*
				 * In order to save bandwidth, an S3 object listing does not
				 * contain every object in the bucket; after a certain point the
				 * S3ObjectListing is truncated, and further pages must be
				 * obtained with the AmazonS3Client.listNextBatchOfObjects()
				 * method.
				 */
				ObjectListing objects = s3.listObjects(bucket.getName());
				do {
					for (S3ObjectSummary objectSummary : objects
							.getObjectSummaries()) {
						totalSize += objectSummary.getSize();
						totalItems++;
					}
					objects = s3.listNextBatchOfObjects(objects);
				} while (objects.isTruncated());
			}

			System.out.println("You have " + buckets.size()
					+ " Amazon S3 bucket(s), " + "containing " + totalItems
					+ " objects with a total size of " + totalSize + " bytes.");
		} catch (AmazonServiceException ase) {
			/*
			 * AmazonServiceExceptions represent an error response from an AWS
			 * services, i.e. your request made it to AWS, but the AWS service
			 * either found it invalid or encountered an error trying to execute
			 * it.
			 */
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			/*
			 * AmazonClientExceptions represent an error that occurred inside
			 * the client on the local host, either while trying to send the
			 * request to AWS or interpret the response. For example, if no
			 * network connection is available, the client won't be able to
			 * connect to AWS to execute a request and will throw an
			 * AmazonClientException.
			 */
			System.out.println("Error Message: " + ace.getMessage());
		}
	}
}
