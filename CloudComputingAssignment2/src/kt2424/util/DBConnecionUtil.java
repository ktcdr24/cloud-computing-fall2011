package kt2424.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnecionUtil {
	
	private static Connection DB_CONNECTION = null;
	private static Boolean isPrevConnectionClosed = true;
	
	private DBConnecionUtil(){
		
	}
	
	public static Connection getConnection(){
		DBConnecionUtil dbConnecionUtil = new DBConnecionUtil();
		if(DB_CONNECTION == null || isPrevConnectionClosed == true){
			DB_CONNECTION = dbConnecionUtil.connectToDatabase();
			isPrevConnectionClosed = false;
		}
		return DB_CONNECTION;
	}
	
	public static void closeConnection(){
		if(DB_CONNECTION != null){
			try {
				DB_CONNECTION.close();
				isPrevConnectionClosed = true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * returns connection to the database cloudcomputingassign2 with given username and pwd 
	 * @param user
	 * @param pwd
	 * @return
	 */
	private Connection connectToDatabase(String user, String pwd){
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloudcomputingassign2",user,pwd);
			System.out.println("con success");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	/**
	 * returns connection to cloudcomputingassign2
	 * @return
	 */
	private Connection connectToDatabase(){
		return connectToDatabase("root", "");
	}

	
//	public static void initializeDatabase(){
//	Connection con = null;
//	try {
//		Class.forName("com.mysql.jdbc.Driver");
//		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloudcomputingassign2","root","");
//		
//	} catch (ClassNotFoundException e) {
//		e.printStackTrace();
//	} catch (SQLException e) {
//		e.printStackTrace();
//	}
//	}
}
