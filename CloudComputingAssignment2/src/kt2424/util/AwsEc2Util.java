package kt2424.util;

import kt2424.amazon.AwsEC2App;
import kt2424.objects.User;

public class AwsEc2Util {

	private static AwsEC2App awsEC2App;

	
	static{
		awsEC2App = new AwsEC2App();
	}
	
	/**
	 * static wrapper for creating user instance.
	 * @param user
	 */
	public static void createInstanceForUser(User user){
		awsEC2App.createInstanceForUser(user);
	}
	
	public static void deleteInstanceForUser(User user){
		awsEC2App.deleteInstance(user);
	}
	
	public static void logIntoVM(User user){
		awsEC2App.logIntoVM(user);
	}
	
}
