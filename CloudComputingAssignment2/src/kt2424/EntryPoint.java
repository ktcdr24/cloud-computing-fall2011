package kt2424;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import kt2424.objects.Admin;
import kt2424.objects.User;

public class EntryPoint {

	public static void main(String[] args) {
		EntryPoint ep = new EntryPoint();
		try {
			ep.login();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void login() throws IOException{
		System.out.println("Do you want login as admin or user?");
		System.out.println("(press 'a' for admin and 'u' for user, 'e' to exit)");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String loginPreference = br.readLine();
		if(!(loginPreference.equalsIgnoreCase("a") || loginPreference.equalsIgnoreCase("u") || loginPreference.equalsIgnoreCase("e"))){
			 System.out.println("invalid input");
			 System.exit(0);
		} else if(loginPreference.equalsIgnoreCase("e")){
			System.out.println("Thank you. Have a nice day.");
		}
		preformLogin(loginPreference);
	}
	
	
	private void preformLogin(String loginPreference) throws IOException{
		if(loginPreference.equalsIgnoreCase("a")){
			loginAsAdmin();
		}else if(loginPreference.equalsIgnoreCase("u")){
			loginAsUser();
		} 
	}
	
	private void loginAsAdmin() throws IOException{
		System.out.println("Please enter admin password");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String adminPassWord = br.readLine();
		Admin admin = new Admin(adminPassWord);
		Boolean isAdminValid = admin.validateAdmin();
		if(isAdminValid){
			while(true){
				System.out.println("Welcome Admin, what do you want to do?");
				System.out.println("1. Register New User");
				System.out.println("2. Delete User");
				System.out.println("3. Print Statistics");
				System.out.println("4. Logout");
				System.out.print("Enter your choice: ");
				int adminChoice = Integer.parseInt(br.readLine());
				switch(adminChoice){
					case 1: admin.registerNewUser();
							break;
					case 2: admin.deleteUser();
							break;
					case 3: admin.printStats();
							break;
					case 4: 
					default: break;
				}	
				if(adminChoice == 4){
					System.out.println("Logging out of Admin");
					break;
				}
			}
			admin = null;
		} else {
			System.out.println("Admin password is wrong");
		}
	}
	
	private void loginAsUser() throws IOException{
		User user = new User();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Please Enter your Username: ");
		user.setUserName(br.readLine());
		System.out.print("Please Enter your Password: ");
		user.setPassWord(br.readLine());
		if(!user.validate()){
			System.out.println("Not authorized. If you are an existing user, your Username/Password combination is wrong (or) If you are a new user, please contact Admin and register yourself. ");
		} else {
			while(true){
				System.out.println(" Welcome user. Please choose from one of the following. ");
				System.out.println(" 1. Login to your virtual machine");
				System.out.println(" 2. Logout");
				System.out.print("Enter your choice: ");
				int userChoice = Integer.parseInt(br.readLine());
				switch(userChoice){
					case 1: user.logIntoVM();
							break;
					case 2: 
					default: break;
				}	
				if(userChoice == 2){
					System.out.println("Logging out of User");
					break;
				}
			}
		}
		
	}
}
