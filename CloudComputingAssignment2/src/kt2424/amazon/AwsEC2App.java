package kt2424.amazon;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import kt2424.objects.User;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AllocateAddressResult;
import com.amazonaws.services.ec2.model.AssociateAddressRequest;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.CreateImageRequest;
import com.amazonaws.services.ec2.model.CreateImageResult;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSnapshotRequest;
import com.amazonaws.services.ec2.model.CreateSnapshotResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.DeleteVolumeRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DetachVolumeRequest;
import com.amazonaws.services.ec2.model.DetachVolumeResult;
import com.amazonaws.services.ec2.model.DisassociateAddressRequest;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.Placement;
import com.amazonaws.services.ec2.model.ReleaseAddressRequest;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Snapshot;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.VolumeAttachmentState;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AwsEC2App {
	
	static AmazonEC2 ec2;
	static AmazonS3Client s3;
	static AmazonCloudWatchClient cloudWatch;
	
	private void init() throws Exception {
	        AWSCredentials credentials = new PropertiesCredentials(
	                AwsEC2App.class.getResourceAsStream("AwsCredentials.properties"));

	        ec2 = new AmazonEC2Client(credentials);
	        s3  = new AmazonS3Client(credentials);
	        // create CloudWatch client
			cloudWatch = new AmazonCloudWatchClient(credentials) ;
	    }

	
	public void createInstanceForUser(User user){
		try {
			init();
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(0);
		}
		System.out.println("[AWS] Start creating instance for " + user);
		try{
			
			/* Create Security Key for User */
			createKeyPair(user);
			/* END Create Security Key for User */
			
			/* create  runInstanceRequest Object */
			requestRunInstance(user, false);
			Thread.sleep(5000);
			/* tag the created instance for easy indentification */
			createTags(user);

			/* Allocate elastic IP addresses  */
			requestIpAddressAllocation(user);
			
			/* wait till the created instance starts. */
        	waitForInstanceToStart(user);
        	
        	/* Create and Attach Volume to the system */
			requestCreateAndAttachVolume(user);
        	
			/* Associate IP address */
			requetsAssociateIPAddress(user);
			
			System.out.println("[AWS] IpAddressAllcoated for " + user);
             
			//createAndSaveInS3Bucket();

			System.out.println("[AWS] instance created and is up and running. ");
			
			System.out.println("[AWS] wait for 30 sec before terminating. ");
			for(int i=1; i<=30; i++){
				System.out.print(i+ ", ");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println();
//			System.out.println("[AWS] Using cloud watch on the instance. ");
//			while(true){
//				if(useCloudWatch(user)){
//					// if CPU util is less than 5 break and tereminate the machine.
//					break;
//				} 
//			}
//			System.out.println("[AWS] Finished Using cloud watch on the instance. ");
			
			System.out.println("[AWS] terminate the instance. ");
			terminateInstance(user);
			
//			System.out.println("[AWS] Delete the instance. ");
//			deleteInstance(user);
			
//			ec2.shutdown();

		}catch(AmazonServiceException ase){
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	/**
	 * @throws AmazonClientException
	 * @throws AmazonServiceException
	 */
	public void createAndSaveInS3Bucket() throws AmazonClientException,
			AmazonServiceException {
		/* S3 bucket and object */
		 //create bucket
		 String bucketName = "cloud-sample-bucket";
		 s3.createBucket(bucketName);
		 
		 //set key
		 String key = "object-name.txt";
		 
		 //set value
		 File file;
		try {
			file = File.createTempFile("temp", ".txt");
			file.deleteOnExit();
		    Writer writer = new OutputStreamWriter(new FileOutputStream(file));
		    writer.write("This is a sample sentence.\r\nYes!");
		    writer.close();
		    
		    //put object - bucket, key, value(file)
		    s3.putObject(new PutObjectRequest(bucketName, key, file));
		     
		    //get object
		    S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
		    BufferedReader reader = new BufferedReader(
		    		new InputStreamReader(object.getObjectContent()));
		    String data = null;
		    while ((data = reader.readLine()) != null) {
		    	System.out.println(data);
		    }
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


	/**
	 * terminate instance created for this user. 
	 * save system state in EBS ro S3.   
	 * @param user
	 */
	public void terminateInstance(User user){
		try{
			try {
				init();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			
        	/* Detach Volume before shutting down the system */
        	requestDetachVolume(user);

			/* disassociate ip address while terminating */
			requestDisassociateIpAddress(user);
			
			/* create image to be used later for this user 
			 * used only while the user is logging off */
			requestCreateImage(user);
			
			System.out.println("[AWS] wait for 30 sec for Image to get created. ");
			for(int i=1; i<=30; i++){
				System.out.print(i+ ", ");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println();
			
			CreateSnapshotRequest createSnapshotRequest = 
					new CreateSnapshotRequest(user.getVolumeId(), user.getUserName()+"_snap");
			CreateSnapshotResult createSnapshotResult = ec2.createSnapshot(createSnapshotRequest);
			
			Snapshot snapshot = createSnapshotResult.getSnapshot();
			user.setSnapshotId(snapshot.getSnapshotId());
			
			List<String> instanceIds = new ArrayList<String>();
        	instanceIds.add(user.getInstanceId());
        	TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest(instanceIds);
        	ec2.terminateInstances(terminateInstancesRequest);
			
        	System.out.println("[AWS] Instance deleted request successfully sent. It will be deleted shortly. ");
        	
		}catch(AmazonServiceException ase){
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
			System.exit(0);
		}
	}
	
	/**
	 * terminate instance created for this user. 
	 * save system state in EBS ro S3.   
	 * @param user
	 */
	public void deleteInstance(User user){
		try{
			try {
				init();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			
			/* create image to be used later for this user 
			 * used only while the user is logging off */
//			requestCreateImage(user);
			
			/* disassociate ip address while terminating */
			requestDisassociateIpAddress(user);
			
			/* Release Ip Address */
			ReleaseAddressRequest releaseAddressRequest = new ReleaseAddressRequest(user.getIpAddress());
			ec2.releaseAddress(releaseAddressRequest);
			
        	/* Detach Volume before shutting down the system */
        	requestDetachVolume(user);
        	
        	/* Delete KeyPair */
        	deleteKeyPair(user);
        	System.out.println("[AWS] KeyPair successfully deleted.");
        	
        	List<String> instanceIds = new ArrayList<String>();
        	instanceIds.add(user.getInstanceId());
        	TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest(instanceIds);
        	ec2.terminateInstances(terminateInstancesRequest);
        	System.out.println("[AWS] Instance deleted request successfully sent. It will be deleted shortly. ");
        	waitForInstanceToTerminate(user);
        	System.out.println("[AWS] Termination was successfully. ");
        	
        	/* Detach Volume before shutting down the system */
        	requestDetachVolume(user);
        	
        	/* Delete volume */
        	DeleteVolumeRequest deleteVolumeRequest = new DeleteVolumeRequest(user.getVolumeId());
        	ec2.deleteVolume(deleteVolumeRequest);
        	System.out.println("[AWS] Volume successfully deleted.");
        	
		}catch(AmazonServiceException ase){
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		}
	}

	/**
	 * 
	 * @param user
	 */
	private boolean useCloudWatch(User user){
		
		//create request message
		GetMetricStatisticsRequest statRequest = new GetMetricStatisticsRequest();
		
		//set up request message
		statRequest.setNamespace("AWS/EC2"); //namespace
		statRequest.setPeriod(60); //period of data
		ArrayList<String> stats = new ArrayList<String>();
		
		//Use one of these strings: Average, Maximum, Minimum, SampleCount, Sum 
		stats.add("Average"); 
		stats.add("Sum");
		statRequest.setStatistics(stats);
		
		//Use one of these strings: CPUUtilization, NetworkIn, NetworkOut, DiskReadBytes, DiskWriteBytes, DiskReadOperations  
		statRequest.setMetricName("CPUUtilization"); 
		
		
		// set time
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		calendar.add(GregorianCalendar.SECOND, -1 * calendar.get(GregorianCalendar.SECOND)); // 1 second ago
		Date endTime = calendar.getTime();
//		calendar.add(GregorianCalendar.MINUTE, -10); // 10 minutes ago
		calendar.add(GregorianCalendar.SECOND, -10); // 10 secs ago
		Date startTime = calendar.getTime();
		statRequest.setStartTime(startTime);
		statRequest.setEndTime(endTime);
		
		//specify an instance
		ArrayList<Dimension> dimensions = new ArrayList<Dimension>();
		dimensions.add(new Dimension().withName("InstanceId").withValue(user.getInstanceId()));
		statRequest.setDimensions(dimensions);
		
		System.out.println("[AWS] 1 CloudWatch wait for 20 sec. ");
		for(int i=1; i<=20; i++){
			System.out.print(i+ ", ");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
		
		//get statistics
		GetMetricStatisticsResult statResult = cloudWatch.getMetricStatistics(statRequest);
		
		System.out.println("[AWS] 2 CloudWatch wait for 20 sec. ");
		for(int i=1; i<=20; i++){
			System.out.print(i+ ", ");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
		
		//display
		System.out.println(statResult.toString());
		List<Datapoint> dataList = statResult.getDatapoints();
		Double averageCPU = 0.0;
		@SuppressWarnings("unused")
		Date timeStamp = null;
		for (Datapoint data : dataList){
			averageCPU = data.getAverage();
			timeStamp = data.getTimestamp();
			System.out.println("[AWS] Average CPU utlilization for last 10 secs: "+averageCPU);
			System.out.println("[AWS] Totl CPU utlilization for last 10 secs: "+data.getSum());
		}
		
		if(averageCPU<5){
			System.out.println("[AWS] As Avg CPU utilization is less than 5% for the last 10 secs, we are terminating the machine.");
			return true;
		} else {
			System.out.println("[AWS] As Avg CPU utilization is less more 5% for the last 10 secs.");
			return false;
		}
	}

	/**
	 * 
	 * @param user
	 */
	public void logIntoVM(User user){
		
		try {
			init();
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(0);
		}
		System.out.println("[AWS] Start Log into instance for " + user);
		try{
			
			/* create  runInstanceRequest Object */
			requestRunInstance(user, true);
			Thread.sleep(5000);

			/* tag the created instance for easy indentification */
			createTags(user);

			/* wait till the created instance starts. */
        	waitForInstanceToStart(user);
        	
        	/* Create and Attach Volume to the system */
			requestCreateAndAttachVolume(user);
        	
			/* Associate IP address */
			requetsAssociateIPAddress(user);
			
			System.out.println("[AWS] IpAddressAllcoated for " + user);

			System.out.println("[AWS] instance created and is up and running. ");
			
			System.out.println("[AWS] wait for 150 sec. ");
			for(int i=1; i<=150; i=i+10){
				System.out.print(i+ ", ");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println();
			System.out.println("[AWS] Using cloud watch on the instance. ");
			while(true){
				if(useCloudWatch(user)){
					// if CPU util is less than 5 break and tereminate the machine.
					break;
				} 
			}
			System.out.println("[AWS] Finished Using cloud watch on the instance. ");
			
			System.out.println("[AWS] LogOff the instance. ");
			logOffVM(user);
			
		}catch(AmazonServiceException ase){
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * terminate instance created for this user. 
	 * save system state in EBS ro S3.   
	 * @param user
	 */
	public void logOffVM(User user){
		try{
			try {
				init();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			
        	/* Detach Volume before shutting down the system */
        	requestDetachVolume(user);

			/* disassociate ip address while terminating */
			requestDisassociateIpAddress(user);
			
			CreateSnapshotRequest createSnapshotRequest = 
					new CreateSnapshotRequest(user.getVolumeId(), user.getUserName()+"_snap");
			CreateSnapshotResult createSnapshotResult = ec2.createSnapshot(createSnapshotRequest);
			
			Snapshot snapshot = createSnapshotResult.getSnapshot();
			user.setSnapshotId(snapshot.getSnapshotId());
			
			List<String> instanceIds = new ArrayList<String>();
        	instanceIds.add(user.getInstanceId());
        	TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest(instanceIds);
        	ec2.terminateInstances(terminateInstancesRequest);
			
        	System.out.println("[AWS] Instance deleted request successfully sent. It will be deleted shortly. ");
        	
		}catch(AmazonServiceException ase){
			System.out.println("Caught Exception: " + ase.getMessage());
			System.out.println("Reponse Status Code: " + ase.getStatusCode());
			System.out.println("Error Code: " + ase.getErrorCode());
			System.out.println("Request ID: " + ase.getRequestId());
			System.exit(0);
		}
	}
	
	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requetsAssociateIPAddress(User user)
			throws AmazonServiceException, AmazonClientException {
		//associate
		AssociateAddressRequest aar = new AssociateAddressRequest();
		aar.setInstanceId(user.getInstanceId());
		aar.setPublicIp(user.getIpAddress());
		ec2.associateAddress(aar);
	}
	
	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestDetachVolume(User user) throws AmazonServiceException,
			AmazonClientException {
		DetachVolumeRequest dvr = new DetachVolumeRequest();
		dvr.setVolumeId(user.getVolumeId());
		dvr.setInstanceId(user.getInstanceId());
		DetachVolumeResult detachVolumeResult = ec2.detachVolume(dvr);
		
		int count = 0;
		while(true){
			count++;
			if(!VolumeAttachmentState.fromValue(detachVolumeResult.
					getAttachment().getState()).equals(VolumeAttachmentState.Detached)){
				System.out.println("[AWS] waiting for volume to be detached");
				System.out.println("[AWS] wait for 10 sec");
				for(int i=1; i<=10; i++){
					System.out.print(i+ ", ");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println();
			} else if(VolumeAttachmentState.fromValue(detachVolumeResult.
					getAttachment().getState()).equals(VolumeAttachmentState.Detached)){
				break;
			}
			if(count==5){
				break;
			}
		}
		System.out.println("[AWS] Volume successfully detached.");
		
	}
	
	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestDisassociateIpAddress(User user)
			throws AmazonServiceException, AmazonClientException {
		//disassociate
		DisassociateAddressRequest dar = new DisassociateAddressRequest();
		dar.setPublicIp(user.getIpAddress());
		ec2.disassociateAddress(dar);
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void createTags(User user) throws AmazonServiceException,
			AmazonClientException {
		// add tags for easy identification
		List<String> resources = new LinkedList<String>();
		List<Tag> tags = new LinkedList<Tag>();
		resources.add(user.getInstanceId());
		tags.add(new Tag("Source", "for_cloud_assign2"));
		tags.add(new Tag("UserName", user.getUserName()));
		tags.add(new Tag("Name", user.getName()));
		ec2.createTags(new CreateTagsRequest(resources, tags));
		
		System.out.println("[AWS] tags created for easy indentification. ");
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 * @throws InterruptedException
	 */
	private void waitForInstanceToStart(User user)
			throws AmazonServiceException, AmazonClientException,
			InterruptedException {
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
		List<String> describeInstanceIds = new ArrayList<String>();
		describeInstanceIds.add(user.getInstanceId());
		describeInstancesRequest.setInstanceIds(describeInstanceIds);
		DescribeInstancesResult describeInstancesResult = ec2.describeInstances(describeInstancesRequest);
		System.out.println("[AWS] ---describeInstancesResult.getReservations().size()==" + describeInstancesResult.getReservations().size());
		for(Reservation reserv : describeInstancesResult.getReservations()){
			for(Instance inst : reserv.getInstances()){
				while(true){
					// if pending - wait, else - get out of the while loop.
					if(InstanceStateName.fromValue(inst.getState().getName()).equals(InstanceStateName.Pending)){
						System.out.println("[AWS] [" + new Date() + "] Instance "+ inst.getInstanceId() +" is still in '" + inst.getState().getName() + "' state");
						System.out.println("[AWS] Sleeping for 30 seconds ");
						for(int sec=30; sec>0; sec--){
							System.out.print(sec + ", ");
							Thread.sleep(1000);
						}
						System.out.println();
						DescribeInstancesResult instancesResult = ec2.describeInstances(describeInstancesRequest);
						inst = instancesResult.getReservations().get(0).getInstances().get(0);
						continue;
					} else if(InstanceStateName.fromValue(inst.getState().getName()).equals(InstanceStateName.Running)){
						break;
					}
				}
			}
		}
	}
	
	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 * @throws InterruptedException
	 */
	private void waitForInstanceToTerminate(User user)
			throws AmazonServiceException, AmazonClientException {
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
		List<String> describeInstanceIds = new ArrayList<String>();
		describeInstanceIds.add(user.getInstanceId());
		describeInstancesRequest.setInstanceIds(describeInstanceIds);
		DescribeInstancesResult describeInstancesResult = ec2.describeInstances(describeInstancesRequest);
		System.out.println("[AWS] ---describeInstancesResult.getReservations().size()==" + describeInstancesResult.getReservations().size());
		for(Reservation reserv : describeInstancesResult.getReservations()){
			for(Instance inst : reserv.getInstances()){
				while(true){
					// if pending - wait, else - get out of the while loop.
					if(!InstanceStateName.fromValue(inst.getState().getName()).equals(InstanceStateName.Terminated)){
						System.out.println("[AWS] [" + new Date() + "] Instance "+ inst.getInstanceId() +" is still in '" + inst.getState().getName() + "' state");
						System.out.println("[AWS] Sleeping for 30 seconds ");
						for(int sec=30; sec>0; sec--){
							System.out.print(sec + ", ");
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						System.out.println();
						DescribeInstancesResult instancesResult = ec2.describeInstances(describeInstancesRequest);
						inst = instancesResult.getReservations().get(0).getInstances().get(0);
						continue;
					} else if(InstanceStateName.fromValue(inst.getState().getName()).equals(InstanceStateName.Terminated)){
						break;
					}
				}
			}
		}
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestCreateAndAttachVolume(User user)
			throws AmazonServiceException, AmazonClientException {
		/* create a volume and attach it to the instance */
		CreateVolumeRequest cvr = new CreateVolumeRequest();
		cvr.setAvailabilityZone("us-east-1a");
		cvr.setSize(10); //size = 10 gigabytes
		if(user.getSnapshotId()!=null && !user.getSnapshotId().equalsIgnoreCase("")){
			cvr.setSnapshotId(user.getSnapshotId());
		}
		CreateVolumeResult volumeResult = ec2.createVolume(cvr);
		String createdVolumeId = volumeResult.getVolume().getVolumeId();
		
		user.setVolumeId(createdVolumeId);
		
		/* Attaching volume to system */
		AttachVolumeRequest avr = new AttachVolumeRequest();
		avr.setVolumeId(user.getVolumeId());
		avr.setInstanceId(user.getInstanceId());
		avr.setDevice("/dev/sdf");
		ec2.attachVolume(avr);
		
		System.out.println("[AWS] Attached volume for " + user);
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestIpAddressAllocation(User user)
			throws AmazonServiceException, AmazonClientException {
		//allocate
		AllocateAddressResult elasticResult = ec2.allocateAddress();
		String elasticIp = elasticResult.getPublicIp();
		System.out.println("New elastic IP: "+elasticIp);
			
		user.setIpAddress(elasticIp);
		
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestCreateImage(User user) throws AmazonServiceException,
			AmazonClientException {
		CreateImageRequest cir = new CreateImageRequest();
		cir.setInstanceId(user.getInstanceId());
		cir.setName(user.getUserName()+"_ami");
		CreateImageResult createImageResult = ec2.createImage(cir);
		String createdImageId = createImageResult.getImageId();
		
		user.setImageId(createdImageId);
		System.out.println("[AWS] Sent creating AMI request for "+ user +", AMI id="+createdImageId);
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void requestRunInstance(User user, boolean isPrivateAMIAvailable) throws AmazonServiceException,
			AmazonClientException {
		Placement instancePlacement = new Placement("us-east-1a");
		String freeInstanceAMI = "ami-7341831a";
//			String suse64AMI = "ami-e4a3578d";
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.setInstanceType("t1.micro");
		if(isPrivateAMIAvailable){
			runInstancesRequest.setImageId(user.getImageId());
		} else {
			runInstancesRequest.setImageId(freeInstanceAMI);
		}
		runInstancesRequest.setMinCount(1);
		runInstancesRequest.setMaxCount(1);
		runInstancesRequest.setPlacement(instancePlacement);
		runInstancesRequest.setKeyName(user.getUserName());
		runInstancesRequest.setMonitoring(true);
		
		/* run the Amazon EC2 instance. */
		RunInstancesResult runInstancesResult = ec2.runInstances(runInstancesRequest);
		
		List<String> createdInstanceIds = new ArrayList<String>();
		if(runInstancesResult.getReservation().getInstances().size() == 1){
			user.setInstanceId(runInstancesResult.getReservation().getInstances().get(0).getInstanceId());
		} else {
			for (Instance instance : runInstancesResult.getReservation().getInstances()) {
				createdInstanceIds.add(instance.getInstanceId());
			}
			user.setInstanceId(createdInstanceIds.get(0));
		}
		
		System.out.println("[AWS] Amazon EC2 Instance with ID=" + user.getInstanceId() + " requested for " + user);
	}

	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void createKeyPair(User user) throws AmazonServiceException,
			AmazonClientException {
		CreateKeyPairRequest keyPairRequest = new CreateKeyPairRequest();
		keyPairRequest.setKeyName(user.getUserName());

		CreateKeyPairResult keyPairResult = ec2.createKeyPair(keyPairRequest);
		String keyPairPEM = keyPairResult.getKeyPair().getKeyMaterial();
		
		user.setKeyPairPEM(keyPairPEM);
		
		System.out.println("[AWS] KeyPair created for "+user);
	}
	
	/**
	 * @param user
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 */
	private void deleteKeyPair(User user) throws AmazonServiceException,
			AmazonClientException {
		DeleteKeyPairRequest deleteKeyPairRequest = new DeleteKeyPairRequest(user.getUserName());
		ec2.deleteKeyPair(deleteKeyPairRequest);
	}
}
