package kt2424.objects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import kt2424.util.AwsEc2Util;
import kt2424.util.DBConnecionUtil;

public class Admin extends User {

	public Admin(String password) {
		super("ADMIN", password);
	}
	
	/**
	 * returns True is password is same as admin password stored in DB. 
	 * otherwise returns False
	 * @return
	 */
	public boolean validateAdmin(){
		if(super.getPassWord().equals("ad")){
			return true;
		}
		return false;
	}
	
	/**
	 * registers new user to the system. 
	 */
	public void registerNewUser(){
		Connection con = DBConnecionUtil.getConnection();
		System.out.println("Enter the number of users you want to enter?");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			PreparedStatement pstmt = con.prepareStatement("insert into user values(?,?,?,?,?,?,?,?,?)");
			int numUsers = Integer.parseInt(br.readLine());
			User newUser = null;
			for(int i=0; i<numUsers; i++){
				newUser = new User();
				System.out.print("Enter username: ");
				newUser.setUserName(br.readLine());
				System.out.print("Enter password: ");
				newUser.setPassWord(br.readLine());
				
				AwsEc2Util.createInstanceForUser(newUser);
				
				pstmt.setString(1, newUser.getUserName());
				pstmt.setString(2, newUser.getPassWord());
				pstmt.setString(3, newUser.getKeyPairPEM());
				pstmt.setString(4, newUser.getName());
				pstmt.setString(5, newUser.getInstanceId());
				pstmt.setString(6, newUser.getImageId());
				pstmt.setString(7, newUser.getVolumeId());
				pstmt.setString(8, newUser.getIpAddress());
				pstmt.setString(9, newUser.getSnapshotId());
				
				pstmt.executeUpdate();
			}
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnecionUtil.closeConnection();		
	}
	
	public void deleteUser(){
		Connection con = DBConnecionUtil.getConnection();
		System.out.print("Enter Username of user you want to delete: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			User user = new User();
			user.setUserName(br.readLine());
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user where username='"+user.getUserName()+"'");
			if(rs.next()){
				user.setInstanceId(rs.getString("instance_id"));
				user.setImageId(rs.getString("image_id"));
				user.setVolumeId(rs.getString("volume_id"));
				user.setKeyPairPEM(rs.getString("key_pair_PEM"));
				user.setIpAddress(rs.getString("ip_address"));
				user.setSnapshotId(rs.getString("snapshot_id"));
			} else {
				System.out.println(" Username not found!!! ");
				return;
			}
			
			AwsEc2Util.deleteInstanceForUser(user);
			System.out.println(" Instance of " + user + " terminated successfully.");
			
			stmt.executeUpdate("Delete from user where username='"+user.getUserName()+"'");
			System.out.println(user + " deleted successfully.");
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * prints the users registered in the system.
	 */
	public void printStats(){
		Connection con = DBConnecionUtil.getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user");
			int count = 0;
			while(rs.next()){
				count++;
				System.out.println("Registered Users >> " + count + ". Username= " + rs.getString("username"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
