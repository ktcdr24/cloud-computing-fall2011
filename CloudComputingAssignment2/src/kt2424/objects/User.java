package kt2424.objects;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import kt2424.util.AwsEc2Util;
import kt2424.util.DBConnecionUtil;

public class User {

	private String userName = "";
	private String passWord = "";
	private String Name = "";
	private String keyPairPEM = "";
	private String instanceId = "";
	private String imageId = "";
	private String volumeId = "";
	private String ipAddress = "";
	private String snapshotId = "";
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public User(String userName, String passWord) {
		this.userName = userName;
		this.passWord = passWord;
	}
	
	
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getKeyPairPEM() {
		return keyPairPEM;
	}
	public void setKeyPairPEM(String keyPairPEM) {
		this.keyPairPEM = keyPairPEM;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	/**
	 * returns true if the user is valid and registered.
	 * otherwise returns false.
	 * @return
	 */
	public Boolean validate(){
		Connection con = DBConnecionUtil.getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user where username='"+this.userName+"' and password='"+this.passWord+"'");
			if(rs.next()){
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * log the user into his virtual machine.
	 */
	public void logIntoVM(){
		Connection con = DBConnecionUtil.getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user where username='"+this.userName+"' and password='"+this.passWord+"'");
			if(rs.next()){
				this.setInstanceId(rs.getString("instance_id"));
				this.setImageId(rs.getString("image_id"));
				this.setVolumeId(rs.getString("volume_id"));
				this.setKeyPairPEM(rs.getString("key_pair_PEM"));
				this.setIpAddress(rs.getString("ip_address"));
				this.setSnapshotId(rs.getString("snapshot_id"));
				AwsEc2Util.logIntoVM(this);
			} else {
				System.out.println(" Username not found!!! ");
				return;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String toString() {
		return "[[User] Username:" + getUserName() + ", ipAddress:" + getIpAddress() + ", voleumeId:" + getVolumeId() + ", imagdId:" + getImageId() + "]";
	}
}
